
// Include express module
const express = require("express");

// Mongoose is a package that allows creation of Schemas to model our data structure
// Also has access to a number of methods for manipulating our database.
const mongoose = require("mongoose")

// Setup the express server
const app = express();

// List the port where the server will listen
const port = 3001;

// MongoDB Connection
// Connect to the database by passing in your connection string
/*
	Syntax: 
		mongoose.connect("<MongoDB Atlas Connection String", {useNewUrlParser: true, useUnifiedTopology: true});
*/

mongoose.connect("mongodb+srv://system:admin@myhelloword.t5o0pyw.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set a notification for connection success or error with our database.
let db = mongoose.connection;

// If a connection error occurred, it will be output in the console.
// console.error.blind(console) allow us to print the error in the browser consoles and in the terminal.
db.on("error", console.error.bind(console, "Connection Error"));

// if the connection is successfull, a console message will be shown.
db.once("open", () => console.log("We're connected to the cloud"));


// Mongoose Schema
// Determine the structure of the document to be written in the database.
// Schemas act as blueprints to our data.
const taskSchema = new mongoose.Schema({
	// Name if the task
	name: String, // String is a shorthand for {type: String}
	status: {
		type: String,
		// Default values are the predefined values for a field if we dont't put any value
		default: "Pending"
	}

});

const userSchema = new mongoose.Schema({
	
	username: String, 
	password: String

});


// Mongoose Model
// Model uses Schema and they act as the middleman from the server (JS code) to our database.

/*
	Syntax: const modelName = mongoose.model("collectionName", mongooseSchema)
*/
	
	// Model must be in singular form and capitalize the first letter
	// Using Mongoose, the package was programmed well enough that it automatically coverts the singular form of the model b=name into a plural form when creating a collection
	const Task = mongoose.model("Task", taskSchema);
	const User = mongoose.model("User", userSchema);


// Middleware Start
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Middleware End


// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

	Task is not duplicated
	- result from the query not eqaul to null
*/

app.post("/tasks", (req,res) =>{

	// Call back function in mongoose methods are programmed this way:
		// first parameter store the error
		// Second parameter
	Task.findOne({name: req.body.name}, (err, result) =>{
		console.log(result);

		// If a document was found anf the document's name matches the information sent by the client/postman
		if (result != null && req.body.name == result.name) {
			// Return a message to the client/postman
			return res.send("Duplicated task found!")
		} else {

			// Create a new task and save to database
			let newTask = new Task({
				name: req.body.name
			})

			// The "save" method will store the information to the database
			// Since the "newTask" was created/instantiaited from the Task model that contains the Mongoose Schema, so it will gain access to the save method

			newTask.save((saveErr, saveTask) => {

				// If there are errors in saving it will be displayed in the console.
				if(saveErr){
					return console.log(saveErr)
				}

				// If no error found while creating the document it will be save in the database.
				else {
					return res.status(201).send("New task created.")
				}
			})

		}
	})

})

// Business Logic
/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req, res) =>{
	Task.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).send(result)
		}
	})
})


app.post("/signup", (req,res) =>{

	if(req.body.username == "" && req.body.password == "" ){

		res.send("Please input Username and Password!")

	}
	else if(req.body.username == "" || req.body.username == undefined) {

		res.send("Please input Username!")

	}
	else if(req.body.password == "" || req.body.password == undefined){

		res.send("Please input Password!")

	}
	else {
		User.findOne({username: req.body.username}, (err, result) =>{
			console.log(result);

			if (result != null && req.body.username == result.username) {
				return res.send(`Username ${req.body.username} already exists in the database!`)
			} 
			else {

				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				})


				newUser.save((saveError, saveUser) => {

					if(saveError){
						return console.log(saveError)
					}

					else {
						return res.status(201).send(`User ${req.body.username} successfully registered!.`)
					}
				})

			}
		})
	}

})





app.get("/registered", (req, res) =>{
	User.find({}, (error, result) =>{
		if(error){
			return console.log(error);
		}
		else{
			return res.status(200).send(result)
		}
	})
})



// Activity
/*

	Instructions s30 Activity:
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S30.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.

*/

//Register a user

/*
	Business Logic:

	1. Add a functionality to check if there are duplicate tasks
		- If the user already exists in the database, we return an error
		- If the user doesn't exist in the database, we add it in the database
	2. The user data will be coming from the request's body
		- Note: Make sure that the username and password from the request is not empty.
	3. Create a new User object with a "username" and "password" fields/properties
*/





// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));